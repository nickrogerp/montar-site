<?php
require "configDB.php";		
$conexao = mysqli_connect("$server", "$user", "$senha", "$banco");
$now = date("Y-m-d H:i:s");	

$date = new DateTime("now", new DateTimeZone('Brazil/East') );
$nowbr= $date->format("d/m/Y H:i:s");

function getDatetimeNow() {
    $tz_object = new DateTimeZone('Brazil/East');
    //date_default_timezone_set('Brazil/East');

    $datetime = new DateTime();
    $datetime->setTimezone($tz_object);
    return $datetime->format('d/m/Y h:i:s');
}	
?>